import 'package:flutter/material.dart';

class CorpP extends StatelessWidget{
  const CorpP({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Корпус П'),
      backgroundColor: const Color.fromARGB(255, 155, 205, 246),),
      body: Card(
      child: Column(
        children: [
          Image.asset(
            'assets/images/P.png',
            ),
          ListTile(
            title: const Text(
              'Марийский государственный университет, педагогический институт',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            subtitle: const Text('Кремлёвская, 44'),
            leading: Icon(
              Icons.restaurant_menu,
              color: Colors.blue[500],
            ),
          ),
          const Divider(),
          ListTile(
            title: const Text('Режим работы: 08:00-17:00'),
            leading: Icon(
              Icons.work_outline,
              color: Colors.blue[500],
            ),
          ),
          ListTile(
            title: const Text(
              '+7 (8362) 64-15-41',
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            leading: Icon(
              Icons.contact_phone,
              color: Colors.blue[500],
            ),
          ),
          ListTile(
            title: const Text('marsu.ru'),
            leading: Icon(
              Icons.contact_mail,
              color: Colors.blue[500],
            ),
          ),
        ],
      ),
    ),
    bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.exit_to_app, color: Colors.black,),
            label: 'Войти',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.call, color: Colors.black,),
            label: 'Связь',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite, color: Colors.black,),
            label: 'Избранное',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.share, color: Colors.black,),
            label: 'Поделиться',
          ),
        ]),
    );
  }
}