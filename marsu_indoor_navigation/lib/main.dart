import 'package:flutter/material.dart';
import 'CorpA.dart';
import 'CorpB.dart';
import 'CorpV.dart';
import 'CorpG.dart';
import 'CorpD.dart';
import 'CorpE.dart';
import 'CorpP.dart';

void main(){
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/':(context) => const MarsuIndoorNav(),
        '/A':(BuildContext context) => const CorpA(),
        '/Б':(BuildContext context) => const CorpB(),
        '/В':(BuildContext context) => const CorpV(),
        '/Г':(BuildContext context) => const CorpG(),
        '/Д':(BuildContext context) => const CorpD(),
        '/E':(BuildContext context) => const CorpE(),
        '/П':(BuildContext context) => const CorpP(),
      },
    )
  );
}

class AppMarsuNav extends StatefulWidget {
  const AppMarsuNav({super.key});

  @override
  State<AppMarsuNav> createState() => AppMarsu();
}

class MarsuIndoorNav extends StatelessWidget {
  const MarsuIndoorNav({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: AppMarsuNav(),
    );
  }
}

class AppMarsu extends State<AppMarsuNav> {
  String selectedPage = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Marsu Indoor Navigation'), 
        backgroundColor: const Color.fromARGB(255, 155, 205, 246),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.search ),
            tooltip: 'Show Snackbar',
            onPressed: () {},
          ),
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {},
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Color.fromARGB(255, 155, 205, 246),
              ),
              child: Text(
                'Выберите корпус (факультет/институт) : ',
                style: TextStyle(
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontSize: 24,
                ),
              ),
            ),
            TextButton.icon(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(Colors.black),
              ),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context){
                      return const CorpA();
                    }
                  ),
                );
              },
              label: const Text(
                'Корпус A (Юридический факультет)',
                 style: TextStyle(fontSize: 20),
                ),
              icon: const Icon(Icons.abc),
            ),
            const Divider(),
            TextButton.icon(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(Colors.black),
              ),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context){
                      return const CorpB();
                    }
                  ),
                );
              },
              label: const Text(
                ('Корпус Б (Институт естественных наук и фармации)'),
                style: TextStyle(fontSize: 20),
              ),
              icon: const Icon(Icons.abc),
            ),
            const Divider(),
            TextButton.icon(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(Colors.black),
              ),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context){
                      return const CorpV();
                    }
                  ),
                );
              },
              label: const Text(
                'Корпус В (Аграрно-технологический институт)',
                style: TextStyle(fontSize: 20),
                ),
              icon: const Icon(Icons.abc),
            ),
            const Divider(),
            TextButton.icon(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(Colors.black),
              ),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context){
                      return const CorpG();
                    }
                  ),
                );
              },
              label: const Text(
                'Корпус Г (Историко-филологический факультет)',
                style: TextStyle(fontSize: 20),
                ),
              icon: const Icon(Icons.abc),
            ),
            const Divider(),
            TextButton.icon(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(Colors.black),
              ),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context){
                      return const CorpD();
                    }
                  ),
                );
              },
              label: const Text(
                'Корпус Д (Медицинский институт)',
                style: TextStyle(fontSize: 20),
                ),
              icon: const Icon(Icons.abc),
            ),
            const Divider(),
             TextButton.icon(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(Colors.black),
              ),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context){
                      return const CorpE();
                    }
                  ),
                );
              },
              label: const Text(
                'Корпус Е (Физико-математический факультет)',
                style: TextStyle(fontSize: 20),
                ),
              icon: const Icon(Icons.abc),
            ),
            const Divider(),
            TextButton.icon(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all(Colors.black),
              ),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context){
                      return const CorpP();
                    }
                  ),
                );
              },
              label: const Text(
                'Корпус П (Педагогический институт)',
                style: TextStyle(fontSize: 20),
                ),
              icon: const Icon(Icons.abc),
            ),
            const Divider(),
          ],
        ),
      ),
      body: Center(
            child: Image.asset(
              'assets/images/logo.png',
              width: 300,
              height: 300,
             ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Color.fromARGB(255, 155, 205, 246),
        child: const Icon(Icons.language)
        ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.reviews),
            label: 'Обзор',
            ),
          BottomNavigationBarItem(
            icon: Icon(Icons.help),
            label: 'Помощь',
            ),
        ]),
    );
  }
}



